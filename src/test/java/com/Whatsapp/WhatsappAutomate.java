package com.Whatsapp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WhatsappAutomate {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "E:\\Chrome Driver\\chromedriver.exe");	
		 
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("debuggerAddress","localhost:9014");
		
		 
		WebDriver driver= new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		WebDriverWait wait=new WebDriverWait(driver, 20);
		
		driver.get("https://web.whatsapp.com/");
		driver.findElement(By.xpath("(//div[@aria-label='Chat list']//following::div//following::span[text()='Rumon Inceptial'])[1]")).click();
		
		WebElement textBox = driver.findElement(By.xpath("//div[text()='Type a message']//following-sibling::div"));
		
		driver.findElement(By.xpath("//div[text()='Type a message']//following-sibling::div")).sendKeys("Hi, Rumon");
		driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		GregorianCalendar time = new GregorianCalendar();
		int hour = time.get(Calendar.HOUR_OF_DAY);
		
		if (hour < 12) {
			textBox.sendKeys("Good Morning");
			driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		}
		else if (hour > 12 && hour < 17) {
			textBox.sendKeys("Good Afternoon");
			driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		}
		else {
			textBox.sendKeys("Good Evening");
			driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		}
		
		driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]/button/span")).click();
		textBox.sendKeys("Reply 1 to schedule meeting or reply 2 to cancel meeting");
		driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]/button/span")).click();
		WebElement reply;
		reply = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("_1Gy50")));
		while(true) {
			String i = reply.getText();
			System.out.println(i);
			if (i.compareTo("1") == 0) {
				textBox.sendKeys("meeting scheduled two hours from now at "+ (hour + 2) + "hours");
				break;
			}
			else if (i.compareTo("2") == 0){
				textBox.sendKeys("meeting canceled");
				break;
			}
			else continue;
		}
		driver.findElement(By.xpath("//*[@id=\"main\"]/footer/div[1]/div/div/div[2]/div[2]/button/span")).click();

		
		
//		driver.findElement(By.xpath("//div[text()='Type a message']//following-sibling::div")).sendKeys("Press 1 to schedule the meeting or Press 2 to cancel the meeting");
//		driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
//		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
//		WebElement message1 = driver.findElement(By.xpath("//span[@aria-label='Rumon Inceptial:']//following::span[text()='1']"));
//		WebElement message2 = driver.findElement(By.xpath("//span[@aria-label='Rumon Inceptial:']//following::span[text()='2']"));
		
//		WebElement message1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[@aria-label='Rumon Inceptial:']//following::span[text()='1'])[1]")));
//		String reply1 = message1.getText();
//		if (reply1 == "1") {
//			driver.findElement(By.xpath("//div[text()='Type a message']//following-sibling::div")).sendKeys("Meeting Scheduled");
//			driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
//			driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
//		}
//		else {System.out.println("error");}
//		
//		WebElement message2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[@aria-label='Rumon Inceptial:']//following::span[text()='2'])[1]")));
//		String reply2 = message2.getText();
//		if (reply2 == "2") {
//			driver.findElement(By.xpath("//div[text()='Type a message']//following-sibling::div")).sendKeys("Meeting Canceled");
//			driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
//			driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
//		}
//		else
//		{System.out.println("Error");}
		
//		System.out.println(reply1);
		
//		System.out.println(reply2);
		
	
//		for (int i = 0; i < 5; i++) {
//			driver.findElement(By.xpath("//div[text()='Type a message']//following-sibling::div")).sendKeys("Hi ");
//			driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
//			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
//			
//			driver.findElement(By.xpath("//div[text()='Type a message']//following-sibling::div")).sendKeys("How are you?");
//			driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
//			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
//			
//			driver.findElement(By.xpath("//div[text()='Type a message']//following-sibling::div")).sendKeys("lets do it");
//			driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
//			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
//			
//			driver.findElement(By.xpath("//div[text()='Type a message']//following-sibling::div")).sendKeys("We will finish it up");
//			driver.findElement(By.xpath("//button[@class='_4sWnG']")).click();
//			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
//			
//		}
//		
		
	}

}
